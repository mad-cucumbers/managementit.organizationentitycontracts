﻿using Contracts.ResponseModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.PositionViewModels
{
    public class PositionByIdResponse
    {
        public PositionViewModel Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}