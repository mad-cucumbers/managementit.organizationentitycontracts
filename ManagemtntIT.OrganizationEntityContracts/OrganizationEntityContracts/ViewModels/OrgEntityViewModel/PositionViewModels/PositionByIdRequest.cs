﻿namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.PositionViewModels
{
    public class PositionByIdRequest
    {
        public int PositionId { get; set; }
    }
}