﻿namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.PositionViewModels
{
    public class DeletePositionRequest
    {
        public int PositionId { get; set; }
    }
}