﻿using System.ComponentModel.DataAnnotations;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.PositionViewModels
{
    public class UpdatePositionViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}