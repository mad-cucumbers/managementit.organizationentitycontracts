﻿using System.Collections.Generic;
using Contracts.ResponseModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.PositionViewModels
{
    public class AllPositionResponse
    {
        public IEnumerable<PositionViewModel> Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}