﻿using System.ComponentModel.DataAnnotations;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.PositionViewModels
{
    public class CreatePositionViewModel
    {
        [Required]
        public string Name { get; set; }
    }
}