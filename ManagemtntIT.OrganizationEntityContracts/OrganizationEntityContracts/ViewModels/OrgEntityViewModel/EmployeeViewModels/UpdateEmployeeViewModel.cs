﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using OrganizationEntityContracts.ViewModels.OrgEntityViewModel.DepartmentViewModels;
using OrganizationEntityContracts.ViewModels.OrgEntityViewModel.PositionViewModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.EmployeeViewModels
{
    public class UpdateEmployeeViewModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Patronymic { get; set; }
        [Required]
        public int DepartamentId { get; set; }
        [Required]
        public int PositionId { get; set; }
        public string WorkTelephone { get; set; }
        [Required]
        public string MobileTelephone { get; set; }
        [Required]
        public string Mail { get; set; }
        public string User { get; set; }

        public List<DepartmentViewModel> SelectDepartment { get; set; }
        public List<PositionViewModel> SelectPosition { get; set; }
    }
}