﻿namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.EmployeeViewModels
{
    public class EmployeeByIdRequest
    {
        public int EmployeeId { get; set; }
    }
}