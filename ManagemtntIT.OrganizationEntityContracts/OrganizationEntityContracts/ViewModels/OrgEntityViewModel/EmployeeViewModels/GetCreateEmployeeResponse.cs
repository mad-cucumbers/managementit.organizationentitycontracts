﻿using Contracts.ResponseModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.EmployeeViewModels
{
    public class GetCreateEmployeeResponse
    {
        public CreateEmployeeViewModel Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}