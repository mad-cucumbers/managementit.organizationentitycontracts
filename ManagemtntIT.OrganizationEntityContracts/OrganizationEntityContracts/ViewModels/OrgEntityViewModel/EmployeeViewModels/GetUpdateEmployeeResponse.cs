﻿using Contracts.ResponseModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.EmployeeViewModels
{
    public class GetUpdateEmployeeResponse
    {
        public UpdateEmployeeViewModel Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}