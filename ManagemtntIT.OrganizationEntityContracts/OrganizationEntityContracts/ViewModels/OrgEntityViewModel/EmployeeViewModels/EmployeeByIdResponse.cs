﻿using Contracts.ResponseModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.EmployeeViewModels
{
    public class EmployeeByIdResponse
    {
        public EmployeeViewModel Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}