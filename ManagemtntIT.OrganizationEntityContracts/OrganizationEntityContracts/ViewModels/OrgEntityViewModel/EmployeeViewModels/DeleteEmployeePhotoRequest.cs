﻿namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.EmployeeViewModels
{
    public class DeleteEmployeePhotoRequest
    {
        public int EmployeeId { get; set; } 
    }
}