﻿namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.EmployeeViewModels
{
    public class GetUpdateEmployeeRequest
    {
        public int EmployeeId { get; set; }
    }
}