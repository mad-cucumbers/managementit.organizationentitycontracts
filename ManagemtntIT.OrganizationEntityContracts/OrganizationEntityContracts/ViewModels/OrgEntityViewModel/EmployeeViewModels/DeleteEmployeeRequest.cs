﻿namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.EmployeeViewModels
{
    public class DeleteEmployeeRequest
    {
        public int EmployeeId { get; set; }
    }
}