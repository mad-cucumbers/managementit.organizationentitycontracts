﻿using System.Collections.Generic;
using Contracts.ResponseModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.EmployeeViewModels
{
    public class AllEmployeeResponse
    {
        public IEnumerable<EmployeeViewModel> Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}