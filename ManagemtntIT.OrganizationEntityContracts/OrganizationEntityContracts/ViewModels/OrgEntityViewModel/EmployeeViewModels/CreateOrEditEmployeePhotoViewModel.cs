﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.EmployeeViewModels
{
    public class CreateOrEditEmployeePhotoViewModel
    {
        public int EmployeeId { get; set; }
        public IFormFile Photo { get; set; }
        public byte[] BytesPhoto { get; set; }
    }
}