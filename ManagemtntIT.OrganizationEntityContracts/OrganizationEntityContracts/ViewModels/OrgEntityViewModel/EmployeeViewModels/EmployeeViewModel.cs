﻿using OrganizationEntityContracts.ViewModels.OrgEntityViewModel.DepartmentViewModels;
using OrganizationEntityContracts.ViewModels.OrgEntityViewModel.EmployeePhotoViewModels;
using OrganizationEntityContracts.ViewModels.OrgEntityViewModel.PositionViewModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.EmployeeViewModels
{
    public class EmployeeViewModel
    {
        public int Id { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Patronymic { get; set; }
        public DepartmentViewModel Departament { get; set; }
        public PositionViewModel Position { get; set; }
        public string WorkTelephone { get; set; }
        public string MobileTelephone { get; set; }
        public string Mail { get; set; }
        public string User { get; set; }
        public EmployeePhotoViewModel Photo { get; set; }
    }
}
