﻿using Contracts.ResponseModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.DepartmentViewModels
{
    public class DepartmentByIdResponse
    {
        public DepartmentViewModel Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}