﻿using Contracts.ResponseModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.DepartmentViewModels
{
    public class GetUpdateDepartmentResponse
    {
        public UpdateDepartmentViewModel Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}