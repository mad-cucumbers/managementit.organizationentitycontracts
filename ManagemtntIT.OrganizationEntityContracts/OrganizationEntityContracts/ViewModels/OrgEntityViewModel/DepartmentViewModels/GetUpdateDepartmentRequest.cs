﻿namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.DepartmentViewModels
{
    public class GetUpdateDepartmentRequest
    {
        public int DepartmentId { get; set; }
    }
}