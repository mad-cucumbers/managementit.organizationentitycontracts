﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using OrganizationEntityContracts.ViewModels.OrgEntityViewModel.SubdivisionViewModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.DepartmentViewModels
{
    public class CreateDepartmentViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public int SubdivisionId { get; set; }

        public List<SubdivisionViewModel> SelectedSubdivision { get; set; }
    }
}