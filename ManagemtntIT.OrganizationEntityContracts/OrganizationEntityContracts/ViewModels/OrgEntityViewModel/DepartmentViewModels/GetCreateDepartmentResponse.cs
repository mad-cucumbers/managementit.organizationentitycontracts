﻿using System.Collections.Generic;
using Contracts.ResponseModels;
using OrganizationEntityContracts.ViewModels.OrgEntityViewModel.SubdivisionViewModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.DepartmentViewModels
{
    public class GetCreateDepartmentResponse
    {
        public CreateDepartmentViewModel Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}