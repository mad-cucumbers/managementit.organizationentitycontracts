﻿using System.Collections.Generic;
using Contracts.ResponseModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.DepartmentViewModels
{
    public class AllDepartmentResponse
    {
        public IEnumerable<DepartmentViewModel> Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}