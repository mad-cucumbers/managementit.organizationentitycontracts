﻿namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.DepartmentViewModels
{
    public class DeleteDepartmentRequest
    {
        public int DepartmentId { get; set; }
    }
}