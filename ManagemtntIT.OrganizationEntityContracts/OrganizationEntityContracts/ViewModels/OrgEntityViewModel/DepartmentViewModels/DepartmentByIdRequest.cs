﻿namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.DepartmentViewModels
{
    public class DepartmentByIdRequest
    {
        public int DepartmentId { get; set; }
    }
}