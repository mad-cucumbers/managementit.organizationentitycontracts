﻿using System.Collections.Generic;
using Contracts.ResponseModels;
using OrganizationEntityContracts.ViewModels.OrgEntityViewModel.DepartmentViewModels;
using OrganizationEntityContracts.ViewModels.OrgEntityViewModel.EmployeeViewModels;
using OrganizationEntityContracts.ViewModels.OrgEntityViewModel.RoomViewModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.Application
{
    public class GetCreateForApplicationResponse
    {
        public List<RoomViewModel> SelectRoom { get; set; }
        public List<DepartmentViewModel> SelectDepartment { get; set; }
        public List<EmployeeViewModel> SelectEmployee { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}