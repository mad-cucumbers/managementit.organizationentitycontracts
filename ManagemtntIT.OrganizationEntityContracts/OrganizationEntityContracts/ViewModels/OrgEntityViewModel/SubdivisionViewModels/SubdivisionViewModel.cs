﻿namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.SubdivisionViewModels
{
    public class SubdivisionViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
