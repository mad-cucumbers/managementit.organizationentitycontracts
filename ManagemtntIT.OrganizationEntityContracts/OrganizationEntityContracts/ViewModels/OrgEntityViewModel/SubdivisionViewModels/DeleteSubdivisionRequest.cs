﻿namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.SubdivisionViewModels
{
    public class DeleteSubdivisionRequest
    {
        public int SubdivisionId { get; set; }
    }
}