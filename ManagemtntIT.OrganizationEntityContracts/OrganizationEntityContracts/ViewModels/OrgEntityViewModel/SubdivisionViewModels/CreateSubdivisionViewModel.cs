﻿using System.ComponentModel.DataAnnotations;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.SubdivisionViewModels
{
    public class CreateSubdivisionViewModel
    {
        [Required]
        public string Name { get; set; }
    }
}