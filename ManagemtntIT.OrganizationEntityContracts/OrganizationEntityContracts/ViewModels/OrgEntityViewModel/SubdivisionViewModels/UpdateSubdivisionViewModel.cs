﻿using System.ComponentModel.DataAnnotations;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.SubdivisionViewModels
{
    public class UpdateSubdivisionViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}