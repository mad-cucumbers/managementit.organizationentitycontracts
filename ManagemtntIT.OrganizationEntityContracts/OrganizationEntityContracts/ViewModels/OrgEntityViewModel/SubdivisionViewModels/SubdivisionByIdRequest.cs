﻿namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.SubdivisionViewModels
{
    public class SubdivisionByIdRequest
    {
        public int SubdivisionId { get; set; }
    }
}