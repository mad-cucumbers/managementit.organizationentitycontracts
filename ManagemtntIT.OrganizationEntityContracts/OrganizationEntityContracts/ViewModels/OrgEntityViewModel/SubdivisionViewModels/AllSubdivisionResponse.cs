﻿using System.Collections.Generic;
using Contracts.ResponseModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.SubdivisionViewModels
{
    public class AllSubdivisionResponse
    {
        public IEnumerable<SubdivisionViewModel> Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}