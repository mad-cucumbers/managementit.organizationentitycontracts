﻿using Contracts.ResponseModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.SubdivisionViewModels
{
    public class SubdivisionByIdResponse
    {
        public SubdivisionViewModel Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}