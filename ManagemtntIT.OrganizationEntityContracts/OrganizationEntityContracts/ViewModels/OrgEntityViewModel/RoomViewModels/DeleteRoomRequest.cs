﻿namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.RoomViewModels
{
    public class DeleteRoomRequest
    {
        public int RoomId { get; set; }
    }
}