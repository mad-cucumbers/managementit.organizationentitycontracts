﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using OrganizationEntityContracts.ViewModels.OrgEntityViewModel.BuildingViewModels;
using OrganizationEntityContracts.ViewModels.OrgEntityViewModel.DepartmentViewModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.RoomViewModels
{
    public class CreateRoomViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public int BuildingId { get; set; }
        public int Floor { get; set; }
        [Required]
        public int DepartamentId { get; set; }
        public int RequiredCountSocket { get; set; }
        public int CurrentCountSocket { get; set; }

        public List<BuildingViewModel> SelectBuilding { get; set; }
        public List<DepartmentViewModel> SelectDepartment { get; set; }
    }
}