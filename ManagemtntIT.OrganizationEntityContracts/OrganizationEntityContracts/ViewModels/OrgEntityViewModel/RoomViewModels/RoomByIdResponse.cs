﻿using Contracts.ResponseModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.RoomViewModels
{
    public class RoomByIdResponse
    {
        public RoomViewModel Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}