﻿namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.RoomViewModels
{
    public class GetUpdateRoomRequest
    {
        public int RoomId { get; set; }
    }
}