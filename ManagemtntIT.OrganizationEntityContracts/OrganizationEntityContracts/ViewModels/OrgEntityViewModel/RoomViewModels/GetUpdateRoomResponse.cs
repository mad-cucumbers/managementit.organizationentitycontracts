﻿using Contracts.ResponseModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.RoomViewModels
{
    public class GetUpdateRoomResponse
    {
        public UpdateRoomViewModel Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}