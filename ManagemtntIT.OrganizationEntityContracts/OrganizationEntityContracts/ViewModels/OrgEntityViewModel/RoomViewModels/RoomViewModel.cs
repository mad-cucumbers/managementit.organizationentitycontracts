﻿using OrganizationEntityContracts.ViewModels.OrgEntityViewModel.BuildingViewModels;
using OrganizationEntityContracts.ViewModels.OrgEntityViewModel.DepartmentViewModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.RoomViewModels
{
    public class RoomViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public BuildingViewModel Building { get; set; }
        public int Floor { get; set; }
        public DepartmentViewModel Departament { get; set; }
        public int RequiredCountSocket { get; set; }
        public int CurrentCountSocket { get; set; }
    }
}
