﻿namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.RoomViewModels
{
    public class RoomByIdRequest
    {
        public int RoomId { get; set; }
    }
}