﻿using Contracts.ResponseModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.RoomViewModels
{
    public class GetCreateRoomResponse
    {
        public CreateRoomViewModel Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}