﻿using System.Collections.Generic;
using Contracts.ResponseModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.RoomViewModels
{
    public class AllRoomResponse
    {
        public IEnumerable<RoomViewModel> Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}