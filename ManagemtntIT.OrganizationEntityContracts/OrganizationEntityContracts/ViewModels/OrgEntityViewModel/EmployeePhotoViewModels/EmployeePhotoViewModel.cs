﻿namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.EmployeePhotoViewModels
{
    public class EmployeePhotoViewModel
    {
        public int Id { get; set; }
        public byte[] Photo { get; set; }

    }
}
