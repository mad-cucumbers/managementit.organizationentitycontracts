﻿namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.BuildingViewModels
{
    public class BuildingByIdRequest
    {
        public int BuildingId { get; set; }
    }
}