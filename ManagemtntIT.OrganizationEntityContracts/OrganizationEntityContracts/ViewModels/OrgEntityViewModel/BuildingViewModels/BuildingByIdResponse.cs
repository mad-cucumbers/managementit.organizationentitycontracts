﻿using Contracts.ResponseModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.BuildingViewModels
{
    public class BuildingByIdResponse
    {
        public BuildingViewModel Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}