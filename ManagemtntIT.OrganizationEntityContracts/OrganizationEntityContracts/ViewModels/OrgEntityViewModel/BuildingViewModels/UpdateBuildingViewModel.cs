﻿using System.ComponentModel.DataAnnotations;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.BuildingViewModels
{
    public class UpdateBuildingViewModel
    {
        public int Id { get; set; }
        
        [Required]
        public string Name { get; set; }

        [Required]
        public string Address { get; set; }
        public int Floor { get; set; }
    }
}