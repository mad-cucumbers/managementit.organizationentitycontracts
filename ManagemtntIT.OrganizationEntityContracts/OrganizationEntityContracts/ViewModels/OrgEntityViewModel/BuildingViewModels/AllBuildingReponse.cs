﻿using System.Collections.Generic;
using Contracts.ResponseModels;

namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.BuildingViewModels
{
    public class AllBuildingReponse
    {
        public IEnumerable<BuildingViewModel> Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}