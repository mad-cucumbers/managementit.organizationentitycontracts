﻿namespace OrganizationEntityContracts.ViewModels.OrgEntityViewModel.BuildingViewModels
{
    public class DeleteBuildingRequest
    {
        public int BuildingId { get; set; }
    }
}